# Read Me

[![Test](https://github.com/Kozea/Radicale/actions/workflows/test.yml/badge.svg?branch=master)](https://github.com/Kozea/Radicale/actions/workflows/test.yml)
[![Coverage Status](https://coveralls.io/repos/github/Kozea/Radicale/badge.svg?branch=master)](https://coveralls.io/github/Kozea/Radicale?branch=master)

I forked this repository from [https://github.com/Kozea/Radicale](https://github.com/Kozea/Radicale) to create docker images for the raspberry pi (ARM processors).

Radicale is a free and open-source CalDAV and CardDAV server.

For the complete documentation, please visit
[Radicale master Documentation](https://radicale.org/master.html).

# Build docker image

Change *VERSION* to the right version.

~~~bash
docker buildx build --platform=linux/arm . -t lookslikematrix/radicale
docker image tag lookslikematrix/radicale:latest lookslikematrix/radicale:VERSION
docker push lookslikematrix/radicale:latest
docker push lookslikematrix/radicale:VERSION
~~~
